from django.db import models
from django.urls import reverse

# Create your models here.
class Album(models.Model):
    artist=models.CharField(max_length=250)
    album_title=models.CharField(max_length=250)
    album_logo=models.FileField()
    
    def get_absolute_url(self):
        return reverse('home-detail', kwargs={'pk': self.pk}) 
  
class Song(models.Model):
    album=models.ForeignKey(Album,on_delete=models.CASCADE)
    song_title=models.CharField(max_length=250,default=' ')
    is_fav=models.BooleanField(default=False)
        