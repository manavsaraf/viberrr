# Generated by Django 2.0 on 2018-01-01 07:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='song_title',
            field=models.CharField(default=' ', max_length=250),
        ),
    ]
