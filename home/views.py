
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.views import generic
from .models import Album
from django.urls import reverse_lazy

class IndexView(generic.ListView):
     template_name = 'home/index.html'
     context_object_name='all_albums'  

     def get_queryset(self):
           return Album.objects.all() 

class DetailView(generic.DetailView):
     template_name = 'home/details.html' 
     model=Album
        


class AlbumCreate(CreateView):
      
     model=Album
     fields=['artist','album_title']
     
class AlbumUpdate(UpdateView):
     model=Album
     fields=['artist','album_title']

class AlbumDelete(DeleteView):
     model=Album
     success_url=reverse_lazy('home:index')        
     
